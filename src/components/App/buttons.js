import React from "react";

class Buttons extends React.Component {
    renderBtns = (count) => {
        let btns = [];
        for (let i = 0; i < count; i++) {
            const { onClick, selected } = this.props;
            btns.push(
                <input
                    type="radio"
                    onClick={() => {
                        onClick(i);
                    }}
                    name="window"
                    key={i}
                    checked={i === selected ? true : false}
                    readOnly
                />
            );
        }
        return btns;
    };
    render() {
        const { count } = this.props;
        return <form className="btns">{this.renderBtns(count)}</form>;
    }
}

export default Buttons;
