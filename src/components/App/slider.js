import React from "react";
import Buttons from "./buttons";

const { count, map } = React.Children;

class Slider extends React.Component {
    state = {
        id: null,
        clicked: false,
    };

    componentDidMount() {
        const middle = Math.floor(count(this.props.children) / 2);
        this.changeWindow(middle);
        this.changeInterval = setInterval(this.moveRight, 8000);
    }

    componentWillUnmount() {
        clearInterval(this.changeInterval);
    }

    changeWindow = (id) => {
        this.setState({
            id,
        });
    };

    shouldComponentUpdate(prevProps, prevState) {
        return prevState.id !== this.state.id;
    }

    moveRight = () => {
        const countOfChildren = count(this.props.children);
        this.setState(({ id }) => {
            if (countOfChildren <= 1) {
                return;
            } else if (id === countOfChildren - 1) {
                return { id: 0 };
            }
            return { id: ++id };
        });
    };

    moveLeft = () => {
        const countOfChildren = count(this.props.children);
        this.setState(({ id }) => {
            if (countOfChildren <= 1) {
                return;
            } else if (id === 0) {
                return { id: countOfChildren - 1 };
            }
            return { id: --id };
        });
    };

    render() {
        const { children } = this.props;
        const { id } = this.state;
        const countOfChildren = count(children);
        if (!countOfChildren) {
            return <h1>No slides</h1>;
        }

        return (
            <div className="slider">
                {map(children, (child, childId) => {
                    if (childId === id) {
                        return child;
                    }
                })}
                <input
                    type="button"
                    className="left-arrow"
                    onClick={this.moveLeft}
                />
                <input
                    type="button"
                    className="right-arrow"
                    onClick={this.moveRight}
                />
                {countOfChildren > 1 ? (
                    <Buttons
                        onClick={this.changeWindow}
                        count={countOfChildren}
                        selected={id}
                    />
                ) : null}
            </div>
        );
    }
}

export default Slider;
