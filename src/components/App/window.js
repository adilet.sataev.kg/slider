import React from "react";

class Window extends React.Component {
    render() {
        const { item } = this.props;
        return (
            <div
                className="window"
                style={{
                    background: `url('${item.image}') no-repeat center center`,
                }}
            >
                <div className="inner">
                    <h1>{item.title}</h1>
                    <p>{item.description}</p>
                </div>
            </div>
        );
    }
}

export default Window;
